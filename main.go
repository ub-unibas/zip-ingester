package main

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

const maxGoroutines = 50

type ZipIngester struct {
	client *http.Client
	log    *os.File
}

func (zi *ZipIngester) getData(endpoint string) ([]FilePath, error) {
	var files []FilePath
	var err error
	switch {
	case strings.HasPrefix(endpoint, "http://") || strings.HasPrefix(endpoint, "https://"):
		files, err = zi.getFromHttp(endpoint)
		if err != nil {
			return nil, err
		}
	case filepath.IsAbs(endpoint):
		files, err = zi.getFromFile(endpoint)
		if err != nil {
			return nil, err
		}
	}
	return files, nil
}

func (zi *ZipIngester) getFromHttp(endpoint string) ([]FilePath, error) {
	var result map[string][]FilePath
	_, err := zi.request(GET, endpoint, nil, &result)
	if err != nil {
		return nil, err
	}
	var flattendData []FilePath
	for _, files := range result {
		flattendData = append(flattendData, files...)
	}
	return flattendData, nil
}

func (zi *ZipIngester) getFromFile(endpoint string) ([]FilePath, error) {
	var data []map[string][]FilePath
	fileContent, err := os.ReadFile(endpoint)
	if err != nil {
		return nil, err
	}
	jsonString := string(fileContent)
	if err := json.Unmarshal([]byte(jsonString), &data); err != nil {
		return nil, err
	}

	var flattendData []FilePath
	for _, entry := range data {
		for _, items := range entry {
			if len(items) > 0 {
				collectionName := items[0].Collection
				if err := zi.checkIfCollectionExists(collectionName); err == nil {
					if err = zi.createCollection(collectionName, "langzeitarchiv"); err != nil {
						// if err = zi.createCollection(collectionName, "test"); err != nil {
						return nil, err
					}
				}
			}
			flattendData = append(flattendData, items...)
		}
	}
	return flattendData, nil
}

func (zi *ZipIngester) ingest(f FilePath) {
	indexerRequestBody := &CreateItemRequest{
		Signature:      f.Identifier,
		Source:         f.Path,
		CollectionName: f.Collection,
		Async:          true,
		Public:         true,
		PublicActions:  nil,
		IIIF:           false,
	}
	byte_json, _ := json.Marshal(indexerRequestBody)
	// var addr = "https://media.ub.unibas.ch/api/items"
	var addr = "https://localhost:8081/api/items"
	result, err := zi.request(POST, addr, byte_json, CreateResponse{})
	// _, err := zi.request(POST, addr, byte_json, CreateResponse{})
	if err != nil {
		log.Printf("[ERROR] %s", fmt.Sprintf("error: %v\n", err))
	}

	log.Printf("[INFO] %s", fmt.Sprintf("%v end ingest: %v result: %s", time.Now(), err, result))
}

func (zi *ZipIngester) getCollection(collectionName string) error {
	// var addr = "https://media.ub.unibas.ch/collections/" + collectionName
	var addr = "https://localhost:8081/collections/" + collectionName
	_, err := zi.request(GET, addr, nil, GetCollectionResponse{})
	if err != nil {
		return err
	}
	return nil
}

func (zi *ZipIngester) checkIfCollectionExists(collecitonName string) error {
	if err := zi.getCollection(collecitonName); err != nil {
		return err
	}
	return nil
}

func (zi *ZipIngester) createCollection(collectionName string, estateName string) error {
	ccr := CreateCollectionRequest{StorageName: "langzeitarchiv", Name: collectionName, Description: "", SignaturePrefix: "", JwtKey: ""}
	// ccr := CreateCollectionRequest{StorageName: "testing3", Name: collectionName, Description: "", SignaturePrefix: "", JwtKey: ""}
	byte_json, _ := json.Marshal(ccr)
	// var addr = fmt.Sprintf("https://media.ub.unibas.ch/api/estates/%s/collections/", estateName)
	// var addr = fmt.Sprintf("https://localhost:8080/api/estates/%s/collections", estateName)
	var addr = fmt.Sprintf("https://localhost:8081/api/estates/%s/collections", estateName)
	_, err := zi.request(POST, addr, byte_json, byte_json)
	if err != nil {
		return err
	}
	return nil
}

func (zi *ZipIngester) request(method HttpMethod, addr string, byte_json []byte, result interface{}) (interface{}, error) {
	req, err := http.NewRequest(string(method), addr, bytes.NewBuffer(byte_json))
	if err != nil {
		log.Printf("[ERROR] %s", fmt.Sprintf("error: %v\n", err))
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", "swordfish"))
	resp, err := zi.client.Do(req)
	if err != nil {
		log.Printf("[ERROR] %s", fmt.Sprintf("error: %v\n", err))
		return nil, err
	}
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		log.Printf("[ERROR] %s", fmt.Sprintf("error: %v\n", err))
		return nil, err
	}
	return result, nil
}

func (zi *ZipIngester) processData(data []FilePath) {
	var wg sync.WaitGroup
	taskCh := make(chan FilePath)
	for i := 0; i < maxGoroutines; i++ {
		go func() {
			for data := range taskCh {
				if data.Path != "" {
					log.Printf("[INFO] %s", fmt.Sprintf("collection: %s path: %s\n", data.Collection, data.Path))
					// go zi.ingest(data)
					zi.ingest(data)
				}
				wg.Done()
			}
		}()
	}

	for _, d := range data {
		wg.Add(1)
		taskCh <- d
	}

	wg.Wait()
	close(taskCh)
}

func main() {
	// var endpoint = "/home/jan/test_data/filesPathKVfile.json"
	// var endpoint = "/home/jan/test_data/filesPathKVfileBadgerRead.json"
	// var endpoint = "/home/jan/test_data/fixedPaths.json"
	// var endpoint = "/home/jan/test_data/test2.json"
	// var endpoint = "http://localhost:8081/data"
	// var endpoint = "/home/jan/test_data/furJan20230724.json"
	var endpoint = "/home/jan/test_data/filepathfrommetsencode_20230926.json"

	caCert, err := os.ReadFile("/home/jan/certs/local/ca.crt")
	// caCert, err := os.ReadFile("/root/certs/ca.cert.pem")
	// caCert, err := os.ReadFile("/home/jan/certs/test/ca.cert.pem")

	if err != nil {
		panic(err)
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	cert, _ := tls.LoadX509KeyPair("/home/jan/certs/local/client.crt", "/home/jan/certs/local/client.key")
	// cert, _ := tls.LoadX509KeyPair("/home/jan/certs/test/ub-mediasrv22.ub.p.unibas.ch.cert.pem", "/home/jan/certs/test/ub-mediasrv22.ub.p.unibas.ch.key.pem")
	// cert, _ := tls.LoadX509KeyPair("/root/certs/ub-mediasrv22.ub.p.unibas.ch.cert.pem", "/root/certs/ub-mediasrv22.ub.p.unibas.ch.key.pem")

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs:            caCertPool,
				InsecureSkipVerify: true,
				Certificates:       []tls.Certificate{cert},
			},
		},
	}

	file, err := os.OpenFile("ingest_log.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		panic("couldn't open log file!")
	}
	defer file.Close()

	log.SetOutput(file)

	zipIngester := &ZipIngester{client: client, log: file}

	data, err := zipIngester.getData(endpoint)
	if err != nil {
		fmt.Printf("error: %v", err)
	}

	zipIngester.processData(data)
}
