package main

type FilePath struct {
	Identifier     string `json:"identifier,omitempty"`
	Collection     string `json:"collection,omitempty"`
	Path           string `json:"path,omitempty"`
	Href           string `json:"href,omitempty"`
	AlmaIdentifier string `json:"alma_identifier,omitempty"`
	DOI            string `json:"doi,omitempty"`
	Sysnum         string `json:"sysnum,omitempty"`
}

type Entries struct {
	Key   string     `json:"-"`
	Items []FilePath `json:""`
}

type Collection struct {
	Name            string `json:"name"`
	EstateId        int64  `json:"estateid"`
	StorageId       int64  `json:"storageid"`
	SignaturePrefix string `json:"signatureprefix"`
	Description     string `json:"description"`
	Secret          string `json:"secret"`
}

type GetCollectionResponse struct {
	Val Collection `json:"Val"`
	Err string     `json:"Err"`
}

type Files struct {
	Files []*FilePath
}

type CreateItemRequest struct {
	Signature      string   `json:"signature"`
	Source         string   `json:"source"`
	CollectionName string   `json:"collectionname"`
	Async          bool     `json:"async"`
	Public         bool     `json:"public"`
	PublicActions  []string `json:"publicactions"`
	IIIF           bool     `json:"iiif"`
}

type CreateResponse struct {
	Status       int    `json:"int"`
	Signature    string `json:"signature"`
	ErrorMessage string `json:"string"`
}

type CreateCollectionRequest struct {
	StorageName     string `json:"storagename"`
	Name            string `json:"name"`
	Description     string `json:"description"`
	SignaturePrefix string `json:"signatureprefix"`
	JwtKey          string `json:"jwtkey"`
}

type HttpMethod string

const (
	POST HttpMethod = "POST"
	GET  HttpMethod = "GET"
)
